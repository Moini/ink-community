## Vortrag: Inkscape-Community (own.your.tools)
Die Inkscape-Community
(own.your.tools)

• Vorstellung der Community: 
   ◇ Ziele: einen quelloffenen Vektorgrafikeditor zur Verfügung zu stellen, dessen Dateiformat zum SVG-Standard kompatibel ist, der die Vorarbeit anderer effizient nutzt und dessen Entwicklungscommunity auch zu diesen Standards beiträgt und der eine lebendige Community hat (ursprünglicher Gründungsgrund / Abspaltung vom Ursprungsprojekt war, dass kaum jemand direkt zum Code beitragen durfte)
   ◇ "fully compliant SVG editor, open developer access to the codebase, as well as using and contributing back to 3rd party libraries and standards such as HIG, CSS, etc. in preference to custom solutions
   ◇ Größe: 
	   ◇ schlecht zu definieren, Grenzen schwammig. 
	   ◇ Letztes Jahr in etwa:
		  ▪ 115 verschiedene Codebeitragende zum Hauptprojekt
		  ▪ 4 Forenmods
		  ▪ 8 Standbetreuer
		  ▪ 10 Aktive im Marketing-Team
		  ▪ 10 regelmäßige Helfer im User-Chat
		  ▪ 25 Codebeitragende zu Erweiterungen
		  ▪ 6 GSOC-Studenten
		  ▪ 1 Outreachy-Studentin
		  ▪ 6 Vorstandsmitglieder
		  ▪ 11 Beitragende zu Tutorials-/Hilfeseite-Projekt
		  ▪ 9 Beitragende zum Webseiten-Projekt
	   ◇ in Summe etwa 150 Beitragende letztes Jahr, davon geschätzt die Hälfte langjährige Projektmitglieder
	   ◇ dazu kommen noch ungezählte unabhängige Autoren von Erweiterungen, Mitglieder in Social-Media-Gruppen, Indonesische Designergruppe, Tutorialvideomacher auf YouTube, ...
   ◇ Länder:
      ▪ Entwicklung zu weniger US-Zentrizität zu beobachten
      ▪ Durch GSOC Zunahme vor allem indischer / pakistanischer Beitragender
      ▪ Kontinente (soweit mir bekannt):
         - viel Europa (DE, FR, GB, IT hauptsächlich)
         - viel USA / Kanada
         - steigend Indien / Pakistan
         - mehrere Südamerika (v.a. Brasilien)
         - mehrere China
         - 1 Australien
         - 2 Nordafrika
   ◇ Geschlechter: 
      ▪ geschätzt > 90% männlich
      ▪ keine Entwicklerinnen in der Stammbesetzung, nur GSOC / Outreachy
      ▪ in anderen Rollen (Marketing, Übersetzung, Support) größerer Frauenanteil
   ◇ Sprache:
      ▪ Lingua Franca ist Englisch, innerhalb der Sprachteams für Übersetzungen ggf. auch jeweilige Sprache
      ▪ Trotz Basis im Chat ist die Kommunikation meist verständlich (es sei denn, es geht um Code), wenig Slang


• Wer verwendet Inkscape und wofür?
   ◇ nur Schätzungen, ausgehend von Supportanfragen (Verzerrung!), es gibt keine validen Daten!
   ◇ von Nutzern werden keine Daten erhoben
   ◇ viele Kleinunternehmen / Vereine (z.B. Logos, Kleinserien von Druckprodukten, von etsy-Shops bis Gartenplaner)
   ◇ viele Hobbyanwender (Grußkarten, Fotomontagen, Geschenke, Heimdeko, Kunst), weil kostenlos + kompatibel mit vielen Ausgabeformaten
   ◇ viele Maker, eigene Communities (z.B. EvilMadScientist, InkStitch, GCodetools - von Flicken über Orthese über Fräsung/Löcher in Gitarre/Akkordeon bis hin zu Modelleisenbahn-Häuschen-Decals)
   ◇ Wissenschaft: Diagramme, Karten, Genetik, Poster, Illustration wissenschaftlicher Texte
   ◇ Webdesign: Mockups, Grafiken zur Weiterverwendung mit JS / CSS


• Organisatorisches (Rechtsform, Teamaufteilung, Arbeitsbereiche, Kommunikationskanäle)
   ◇ Ein Haufen Freiwilliger :) (noch)
   ◇ Rechtsform: 
      ▪ Software Freedom Conservancy als Gemeinnützige Organisation in den USA hält die Rechte an Logo, Domains, Markenzeichen.
      ▪ SFC verwaltet Konten, kümmert sich um Steuerzahlungen, Spendenannahme und -abgabe
      ▪ von den Projektmitgliedern gewählter Vorstand aus z.Zt. 6 Personen (max 7) trifft finanzielle Entscheidungen, greift aber nicht aktiv in die konkrete Softwareentwicklung ein und bestimmt auch nicht deren Ziele.
   ◇ Gewählter Projektvorstand als Mittler
   ◇ Intern verschiedene, locker strukturierte Teams:
      ▪ Entwicklung
      ▪ Marketing  
      ▪ UI / UX
      ▪ Testing
      ▪ Bugmanagement
      ▪ Erweiterungen
      ▪ Webseite
      ▪ Übersetzung
      ▪ Dokumentation


• Motivationen für das Beitragen zu Inkscape / Open Source + Benefits:
   ◇ Unterschied: Anfangen ↔ Dabeibleiben
   ◇ Eigennutz: Sprache, benötigtes Feature, 
                        nerviger Bug
   ◇ Idealismus: Softwarefreiheit, 'Zurückgeben', 
                         sinnvolle Freiwilligenarbeit
   ◇ Lernen: Programmieren, Workflows, Teamwork, 
                  Leitungsaufgaben)
   ◇ Spaß: Gemeinschaft, Rumprobieren, Hobby, 
               Langeweile
   ◇ Ruf: Lebenslauf, Anerkennung
   ◇ Geld: GSOC, Outreachy, Aufträge
   ◇ Umfrage ausstehend: https://office.inkscape.org/nextcloud/index.php/apps/forms/Ft3Y4iGT2b3Pz8ms/results


• Negative Seiten einer Mitgliedschaft in einem Open-Source-Projekt:
   ◇ keine festen Grenzen:
      ▪ Vernachlässigung ‘RL’ (real life)
      ▪ Burnout
   ◇ Aufeinandertreffen verschiedener Kulturen und Erwartungen:
      ▪ Konflikte und Drama
      ▪ Verstärkung, da rein online
   ◇ Freiwilligkeit:  
      ▪ Nervige Aufgaben bleiben lange unerledigt
      ▪ Keine festen Zuständigkeiten
      ▪ Zukünftig evtl. Konkurrenz bezahlt / unbezahlt


• Mit welchen Fähigkeiten kann man Inkscape / Open-Source-Projekte im Allgemeinen unterstützen? 
   ◇ solide Englischkenntnisse
   ◇ grundlegendes Wissen über Inkscape / Open Source
   ◇ Bereitschaft
      ▪ zu lernen
      ▪ sich mit Technik auseinanderzusetzen
      ▪ zu recherchieren
      ▪ auszuprobieren
      ▪ nachzufragen
      ▪ Feedback zu erkennen
      ▪ sich anzupassen
• konkret z.B. :
  Beherrschung einer zusätzl. Sprache, (erste)   
  Programmierkenntnisse, UI / UX, Lust etwas zu 
  organisieren, Spaß am Lehren, Mediengestaltung,   
  Webentwicklung, Dokumentation, Artikelschreiben, Social Media … 

• Was passiert, wenn man mitmachen will - wie fängt man das an? 
   ◇ erstmal beobachten und mitlesen, um ein Gefühl dafür zu bekommen, wie so der Umgang ist
   ◇ sich überlegen, was einem Spaß machen könnte
   ◇ beim entsprechenden Team Hallo sagen, fragen, wo Hilfe gebraucht wird
   ◇ sich mit der Technik vertraut machen
   ◇ mit anderen Kontakt aufnehmen, die an derselben Sache arbeiten
   ◇ eine kleine Aufgabe übernehmen und mit Unterstützung / Review zu Ende führen


• Worauf muss man vorbereitet sein?
   ◇ Nicht alles, was einem selbst als eine super-duper-neue Idee erscheint, ist auch neu, vielleicht wurde es vorher schon begründet abgelehnt, oder es ist bereits eine andere Lösung geplant
   ◇ Wenn man einfach ohne Koordination anfängt, an irgendwas zu arbeiten, ist es möglich, dass diese Arbeit nicht geschätzt wird / unnötig ist / bereits von jemand anderem erledigt wird. Manchmal braucht man auch nur etwas Geduld, bis jemand Zeit hat, sich etwas anzuschauen.
   ◇ Das (oft sehr vorsichtig formulierte) Feedback sollte man ernst nehmen, sonst ist man mit seinem Vorhaben recht schnell allein auf weiter Flur
   ◇ Es wird eine gewisse Selbständigkeit und die Bereitschaft, etwas einfach mal auszuprobieren, erwartet. Zuviel Hilfebedürfnis und zuviel Nachfragen bei Dingen, die man sich leicht selbst erarbeiten kann, strapaziert die anderen Freiwilligen unnötig.
   ◇ Die Atmosphäre ist eher professionell-kollegial, für Online-Gruppen evtl. ungewohnt ('bro', Memes,...). Die langjährigen Beitragenden sind zw. 35 und 60.
   ◇ Am Anfang wird man auf extrem viel Hilfsbereitschaft, Lob und Willkommenheißen treffen. Das lässt aber auch irgendwann nach, wenn man als selbständig genug angesehen wird. Leute sind nicht immer schnell erreichbar.


• Zusammen wachsen
   ◇ Beispiel: User support
      ▪ kommen, um selbst Hilfe zu erhalten
      ▪ bleiben im Chat, weil's nett ist und sie etwas lernen
      ▪ helfen dann anderen
   ◇ Beispiel: Programmieren
      ▪ kommen, um einen bestimmten Bug zu beheben, der sie selbst nervt
      ▪ stoßen auf freundliche Community, erfahren, dass ihr Beitrag geschätzt wird
      ▪ fangen an, auch andere Bugs zu bearbeiten
   ◇ Beispiel: Social 
      ▪ kommen, weil sie bestimmte Elemente im Marketing vermissen
      ▪ übernehmen regelmäßige Aufgabe
      ▪ werden Teil des Teams
      ▪ schauen, wo sie noch mithelfen könnten
   ◇ Langfristig:
      ▪ Übernahme von zunehmend mehr Verantwortung
      ▪ Unterstützung neuer Beitragender
      ▪ Eigeninitiative


• mittelfristige Zukunft von Inkscape:
   ◇ Version 1.1.2 / 1.2 alpha Anfang Februar
   ◇ About-Screen-Contest ab Anfang Februar!
   ◇ Stand Chemnitzer Linuxtage März
   ◇ Version 1.2 im Mai
      ▪ Mehrseitendokumente
      ▪ Gleichmäßige Abstände mit der Maus
      ▪ Ursprung für Transformationen setzen
      ▪ Leerraum um Fließtexte (padding)
      ▪ Alle Ausrichten-Funktionen in einem Dialog
      ▪ Ebenendialog = Objekte-Dialog
      ▪ Farbverlaufseditor ist wieder da!
      ▪ Editierbare Marker + Strichlinien
      ▪ u.v.m.

• und wie weiter?
   ◇ keine feste Roadmap für Features
      ▪ alles hängt von Verfügbarkeit der Beitragenden ab
   ◇ Pläne, jemanden anzustellen
      ▪ um das Spendengeld sinnvoll einzusetzen
      ▪ um unliebsame Organisationsaufgaben zu
    übernehmen
   ◇ Pläne, für bestimmte Features jemanden einzustellen
   ◇ Pläne, allen Community-Mitgliedern das Wahlrecht
  für Vorstandswahlen zu geben




