# Inkscape-Community

Vortragsfolien und Notizen zum Vortrag 'Die Inkscape-Community: Lebendige Open-Source-Kultur'

Lizenz: CC-By-SA 4.0 Maren Hachmann

Eingebundene Bilder bearbeitet durch Maren Hachmann (Entfernung von Texten, Formatanpassung), jeweils lizenziert als CC-By-SA.

Originalautoren:

* Fliegende Insel: [Bayu Rizaldhan Rayes](https://inkscape.org/~bayubayu/%E2%98%85island-of-creativity)
* Zeichnende Göttin: [Fabian Mosakowski](https://inkscape.org/~fabianmosakowski/%E2%98%85unleash-your-creativity)
* Zertifikat: [Kabeer](https://gitlab.com/inkscape/vectors/content/-/raw/master/screens/about/0.48/contest_entries/22_inkscape_splashscreen_contest_by_kabeer.svg)
* Treppe: [Spaventapasseri](https://inkscape.org/~spaventapasseri/%E2%98%85stairskape-illusion)
* Erfinder: [Johann](https://inkscape.org/~johannc/%E2%98%85scientist)
* Logo "Gekreuzte Waffen": [SimplyEr1c](https://inkscape.org/~SimplyEr1c/%E2%98%85creative-tools-inkscape-10-contest)
* Kleine Monster: [Nadezhda Sorokina](https://inkscape.org/~Lutik-drawing/%E2%98%85artfriends)
* Hochhauspläne: [Grutensaie](https://gitlab.com/inkscape/vectors/content/-/raw/master/screens/about/0.46/contest_entries/14_inkscape_a_propos_by_grutensaie.svg)
* Batterie: [Fauzan Syukri](https://inkscape.org/~ozant/%E2%98%85inkscape-11-splash-screen)
* Mädchen mit Krake: [Dismecha](https://inkscape.org/~Dismecha/%E2%98%85how-to-fold-a-memory)
* Vergnügter Oktopus: [M Ristiyanto](https://inkscape.org/~masmajnun.studio/%E2%98%85mewarnai-dunia-by-masmajnun)
* Springender Fisch: [JF Barraud](https://gitlab.com/inkscape/vectors/content/-/raw/master/screens/about/0.46/contest_entries/23_about_inkscape_by_jfbarraud.svg)
* Drachenzähmen: [Muhamad Farlly](https://inkscape.org/~mfarlly/%E2%98%85inkscape-funtastic-dragon-i-muhamad-farlly)
* Mädchen mit Schwänen: [Arwassa](https://gitlab.com/inkscape/vectors/content/-/raw/master/screens/about/0.48/contest_entries/03_inkscape_0_48__screen_contest_by_arwassa.svg)
* Küchenszene: [tomatojuice](https://inkscape.org/~tromatojuice/%E2%98%85cooking)
* Blind laufendes Kind: [richard Ibrahim](https://inkscape.org/~richardibra/%E2%98%85inkscape-splash-art)
* Buntes Durcheinander: [Ray Oliveira](https://inkscape.org/~RayOliveira/%E2%98%85desenhe-livremente)
